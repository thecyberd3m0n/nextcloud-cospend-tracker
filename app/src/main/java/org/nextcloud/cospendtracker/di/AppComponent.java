package org.nextcloud.cospendtracker.di;

import org.nextcloud.cospendtracker.LoginDialogActivity;
import org.nextcloud.cospendtracker.MainActivity;
import org.nextcloud.cospendtracker.SettingsActivity;
import org.nextcloud.cospendtracker.services.SyncItemStateService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { ApiModule.class })
public interface AppComponent {
    void injectActivity(LoginDialogActivity activity);
    void injectActivity(MainActivity activity);
    void injectService(SyncItemStateService service);
}
